const express = require('express')
const app = express();

const http = require('http');
const server = http.Server(app);

const socketIO = require('socket.io');
const io = socketIO(server);

const port = process.env.PORT || 3000;

io.on('connection', (socket) => {
    console.log('user connected');
    socket.on('new-message', (message) => {
        // console.log(message);
        io.emit('new-message', message);
        console.log( 'el mensaje emitido: '+message);
    });
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
});

server.listen(port, () => {
    console.log(`started on port: ${port}`);
});